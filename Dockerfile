FROM rust:1.83.0-slim

LABEL maintainer="Nguyễn Hồng Quân <ng.hong.quan@gmail.com>"

RUN apt-get update && apt-get install -y zlib1g libtinfo6 pkg-config libpq-dev libssl-dev libsystemd-dev && rm -rf /var/cache/apt/
